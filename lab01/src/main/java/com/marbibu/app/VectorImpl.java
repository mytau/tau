package com.marbibu.app;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class VectorImpl implements Vector<VectorImpl>{//{{{
  private List<Double>items;
  private int n;
  public VectorImpl getSelf(){//{{{
    return this;
  }//}}}
  public VectorImpl(List<Double>items){//{{{
    this.items=items;
    this.n=items.size();
  }//}}}
  public VectorImpl add(VectorImpl vector){//{{{
    int i=0;
    List<Double>list=vector.getList();
    for(Double item:list){
      this.items.set(i,this.items.get(i)+item);
      i++;
    }
    return this;
  }//}}}
  public String stringify(){//{{{
    String result="";
    int i=0;
    for(Double item:this.items){
      if(i==this.n-1){
        result+=item;
      }else{
        result+=item+",";
      }
      i++;
    }
    return result;
  }//}}}
  public List<Double>getList(){//{{{
    return this.items;
  }//}}}
  public void setList(List<Double>items){//{{{
    this.items=items;
  }//}}}
  public int getN(){//{{{
    return this.n;
  }//}}}
  public static VectorImpl add(VectorImpl v1,VectorImpl v2){//{{{
    List<Double>array=new ArrayList<Double>(),
                list=v1.getList(),
                list2=v2.getList();
    int i=0;
    for(Double item:list){
      array.add(item+list2.get(i));
      i+=1;
    }
    return new VectorImpl(array);
  }//}}}
}//}}}
