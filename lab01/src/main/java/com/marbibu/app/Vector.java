package com.marbibu.app;

import java.util.List;

public interface Vector<SELF extends Vector<SELF>>{//{{{
  public SELF getSelf();
  public SELF add(SELF vector);
  public String stringify();
  public List<Double>getList();
  public int getN();
  public void setList(List<Double>items);
  //nie mozna utworzyc statycznej metody
}//}}}
