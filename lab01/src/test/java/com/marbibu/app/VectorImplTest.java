package com.marbibu.app;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.Test;
//import static org.junit.Assert.*;


public class VectorImplTest{//{{{
  List<VectorImpl>vectors=Arrays.asList(
    new VectorImpl(Arrays.asList(2.0,7.0)),
    new VectorImpl(Arrays.asList(4.0,15.0)),
    new VectorImpl(Arrays.asList(2.0,7.0,10.0)),
    new VectorImpl(Arrays.asList(4.0,15.0,20.0))
  );
  String expected,result;
  List<Double>previousList;
  VectorImpl v1,v2;
  private void __test_add(int index1,int index2,String expected){//{{{
    v1=vectors.get(index1);
    v2=vectors.get(index2);
    previousList=v1.getList();
    v1.add(v2);
    result=v1.stringify();
    v1.setList(previousList);
    assertEquals(expected,result);
  }//}}}
  private void __test_add2vectors(int index1,int index2,String expected){//{{{
    v1=vectors.get(index1);
    v2=vectors.get(index2);
    VectorImpl v=VectorImpl.add(v1,v2);
    result=v.stringify();
    assertEquals(expected,result);
  }//}}}
  @Test
  public void test_add2items(){//{{{
    this.__test_add(0,1,"6.0,22.0");
  }//}}}
  @Test
  public void test_add3items(){//{{{
    this.__test_add(2,3,"6.0,22.0,30.0");
  }//}}}
  @Test
  public void test_add2vectors(){//{{{
    this.__test_add2vectors(0,1,"6.0,22.0");
  }//}}}
  @Test
  public void test_add2vectors_3(){//{{{
    this.__test_add2vectors(2,3,"6.0,22.0,30.0");
  }//}}}
  @Test
  public void test_equalSizes(){//{{{
    v1=vectors.get(0);
    v2=vectors.get(1);
    assert v1.getN()==v2.getN();
  }//}}}
  @Test
  public void test_notEqualSizes(){//{{{
    v1=vectors.get(1);
    v2=vectors.get(2);
    assert v1.getN()!=v2.getN();
  }//}}}
}//}}}
