import os,sys
company="com.marbibu.app"
data=dict(zip(["name","project","archetype"],sys.argv))
cmd="mvn archetype:generate "
cmd+="-DgroupId="+company
cmd+=" -DartifactId="+data["project"]
try:
  cmd+=" -DarchetypeArtifactId="+data["archetype"]
except:
  cmd+=" -DarchetypeArtifactId=maven-archetype-quickstart"
cmd+=" -DinteractiveMode=false"
print(cmd)
os.system(cmd)

file=open("./"+data["project"]+"/.data.json","w")
file.write("{company:'"+company+"',project:'"+data["project"]+"'}")
file.close()

file=open("./"+data["project"]+"/.run.py","w")
file.write("import os\nos.system('mvn package & java -cp target/"+data["project"]+"-1.0-SNAPSHOT.jar "+company+".App')")
file.close()

os.system("cp .scripts/.newInterface.py "+data["project"]+"/.newInterface.py")
os.system("cp .scripts/.newClass.py "+data["project"]+"/.newClass.py")

os.chdir(data["project"])
os.system("mvn package")
