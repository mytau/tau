var socket=io();
function print(text){
  console.log(text);
}
function logIn(event){
  var nick=document.getElementById("nick").value,
      password=document.getElementById("password").value;
  document.getElementById("nick").value="";
  document.getElementById("password").value="";
  
  socket.emit("logIn",{
    nick:nick,
    password:password
  })
}

socket.on("comment",function(data){
  document.getElementById("result").innerHTML=data.comment;
})
