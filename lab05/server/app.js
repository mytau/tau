'use strict'
var PORT=7500
//imports{{{
var express=require('express'),
    bodyParser=require("body-parser"),
    app=express(),
    server=require('http').Server(app),
    url=require('url'),
    io=require('socket.io')(server),
    terminal=require('child_process').execFile,
    fs=require('fs'),
    encoding=require("encoding");

var clientM=new ClientManager();
//}}}
var print=function(text){//{{{
  console.log(text);
}//}}}
//middleware{{{
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('client'));
//}}}
app.get('/',(req,res)=>{//{{{
  res.sendfile(__dirname+"/client/index.html");
})//}}}
io.sockets.on("connection",socket=>{//{{{
  print(`-->Client ${socket.id} was connected.`)
  socket.on("disconnect",()=>{
    print(`-->Client ${socket.id} was disconnected.`)
  })
  socket.on("logIn",(data)=>{
    clientM.logIn(socket,data)
  })
})//}}}
server.listen(PORT,function(){//{{{
  print(`Server starts at localhost:${PORT}`);
});//}}}

function ClientManager(){//{{{
  this.__clientSL={
    marco:{
      password:"polo",
      logged:false
    },
    admin:{
      password:"admin",
      logged:false
    }
  }
}
var def=ClientManager.prototype;
def.logIn=function(socket,data){//{{{
  let obj={};
  if(this.__clientSL.hasOwnProperty(data.nick)){
    if(this.__clientSL[data.nick].logged){
      obj.comment="You are already logged...";
      obj.bool=false;
    }else{
      this.__clientSL[data.nick].logged=true;
      obj.comment="You were logged successfully.";
      obj.bool=true;
    }
  }else{
    obj.comment="You are not registered.";
    obj.bool=false;
  }
  print(`--> Client ${socket.id} logs in with data: ${data}`);
  io.sockets.connected[socket.id].emit("comment",obj);
}//}}}
//}}}
