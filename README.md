![Alt text](https://maven.apache.org/images/maven-logo-black-on-white.png)![Alt text](http://www.logoeps.com/wp-content/uploads/2013/03/java-eps-vector-logo-200x200.png)
# tau - java
####(Testowanie)
##1.Tworzenie projektu maven z terminala:
####  * instalacja maven
####    `brew install maven`
####  * sprawdzenie wersji maven'a
####    `mvn -version`
####  * tworzenie nowego projektu
####    `mvn archetype: generate -DgroupId=com.nazwa_firmy.app -DartfactId=nazwa_aplikacji -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false`
####  * tworzenie nowego projektu (przy użyciu python'a)
####    `python .newProject.py nazwa_projektu nazwa_archetypu`
####  * budowanie projektu
####    `mvn package`
####  * uruchamianie projektu
####    `java -cp target/nazwa_aplikacji-1.0-SNAPSHOT.jar com.nazwa_firmy.app.App`
####  * uruchamianie projektu (przy użyciu python'a)
####    `python .run.py`
####  * uruchomienie sesji w vim'ie
####    `vim -S .mySession.vim`
##2.Tworzenie repozytorium itp.
####  * `git init`
####  * `git remote add origin https://mytau@bitbucket.org/mytau/tau.git`
####  * `git add .`
####  * `git commit -am "..."`
####  * `git push -u origin master` (<-- poki co wszystko na głównego branch'a)
##3.Dodatkowe skrypty pomocnicze
####  * tworzenie interface'u
####    `python .newInterface.py nazwa_interface'u`
####  * tworzenie klasy
####    `python .newClass.py nazwa_klasy`
##4.Ogólne
####  annoying {{{ }}} -> vim
